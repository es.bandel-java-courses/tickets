package org.example.app.repository;

import lombok.RequiredArgsConstructor;
import org.example.app.entity.CommentEntity;
import org.jdbi.v3.core.Jdbi;
import org.springframework.stereotype.Component;

import java.util.List;

@RequiredArgsConstructor
@Component
public class CommentRepository {

    private final Jdbi jdbi;

    public void add(final CommentEntity commentEntity) {
        jdbi.withHandle(handle -> handle.createUpdate(
                        // language=PostgreSQL
                        "INSERT INTO comments (ticketId, text,userlogin) VALUES (:ticketId, :text,:userLogin)")
                .bind("ticketId", commentEntity.getTicketId())
                .bind("text", commentEntity.getText())
                .bind("userLogin", commentEntity.getUserLogin())
                .execute()
        );
    }

    public List<CommentEntity> getByTicketId(final long ticketId) {
        return jdbi.withHandle(handle -> handle.createQuery(
                        // language=PostgreSQL
                        "SELECT * FROM comments WHERE ticketId=:ticketId")
                .bind("ticketId", ticketId)
                .mapToBean(CommentEntity.class)
                .list()
        );

    }
}
