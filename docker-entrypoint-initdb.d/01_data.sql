INSERT INTO users(login,password,role)
VALUES ('dasha','$argon2id$v=19$m=4096,t=3,p=1$q0NUlEUl7WjH/i/Z8v4qgQ$vs4NU+Sw3RnnV/buWyqTQp3FRdCSZstPrgkyTtOMaL4','user'),
       ('masha','$argon2id$v=19$m=4096,t=3,p=1$+dydUGWcqmMZz9z1QMmvGw$D3CiHFRToFXQMnz+HkG4jscPqInFtew447oxJi6Snyg','user');

INSERT INTO users(login,password,role)
VALUES ('liza','$argon2id$v=19$m=4096,t=3,p=1$7RemK/gr/NT8F+VP06eRAQ$AH8r8MEj0i60FGiACOZ/aUMh9nJc8+mGiocO2z20CaE','support');

INSERT INTO users(login,password,role)
VALUES ('andrew','$argon2id$v=19$m=4096,t=3,p=1$q0NUlEUl7WjH/i/Z8v4qgQ$vs4NU+Sw3RnnV/buWyqTQp3FRdCSZstPrgkyTtOMaL4','user'),
       ('arthur','$argon2id$v=19$m=4096,t=3,p=1$+dydUGWcqmMZz9z1QMmvGw$D3CiHFRToFXQMnz+HkG4jscPqInFtew447oxJi6Snyg','user');

