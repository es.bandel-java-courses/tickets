package org.example.app.repository;

import lombok.RequiredArgsConstructor;
import org.example.app.entity.TicketEntity;
import org.jdbi.v3.core.Jdbi;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;


@RequiredArgsConstructor
@Component
public class TicketRepository {
    private final Jdbi jdbi;

    public void add(final TicketEntity ticket) {
        jdbi.withHandle(handle -> handle.createUpdate(
                        // language=PostgreSQL
                        "INSERT INTO tickets( name, content,status,userLogin) VALUES (:name, :content,DEFAULT,:userLogin)")
                .bind("name", ticket.getName())
                .bind("content", ticket.getContent())
                .bind("userLogin", ticket.getUserLogin())
                .execute()
        );
    }

    public void update(final long id) {
        jdbi.withHandle(handle -> handle.createUpdate(
                        // language=PostgreSQL
                        "UPDATE tickets SET status =:status WHERE id=:id")
                .bind("status", "CLOSED")
                .bind("id", id)
                .execute()
        );
    }

    public Optional<TicketEntity> getById(final long id) {
        return jdbi.withHandle(handle -> handle.createQuery(
                        // language=PostgreSQL
                        "SELECT name, content, status,userlogin FROM tickets WHERE id = :id"
                )
                .bind("id", id)
                .mapToBean(TicketEntity.class)
                .findFirst());
    }

    public List<TicketEntity> getAll() {
        return jdbi.withHandle(handle -> handle.createQuery(
                        // language=PostgreSQL
                        "SELECT * FROM tickets")
                .mapToBean(TicketEntity.class)
                .list()
        );
    }
}

