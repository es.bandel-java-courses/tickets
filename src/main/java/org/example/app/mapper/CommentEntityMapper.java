package org.example.app.mapper;

import org.example.app.dto.CommentRQ;
import org.example.app.dto.CommentRS;
import org.example.app.entity.CommentEntity;
import org.mapstruct.Mapper;


@Mapper
public interface CommentEntityMapper {
    CommentEntity fromCommentRQ(
            final CommentRQ request,
            final String userLogin
    );

    CommentRS toCommentRS(final CommentEntity entity);

}