package org.example.app.entity;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class TicketEntity {

    private long id;

    private String name;

    private String content;

    private String status;

    private String userLogin;
}
