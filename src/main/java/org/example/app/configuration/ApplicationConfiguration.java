package org.example.app.configuration;

import com.google.gson.Gson;
import org.example.app.controller.CommentController;
import org.example.app.controller.TicketController;
import org.example.framework.Maps;
import org.example.framework.context.ContextBeanNames;
import org.example.framework.handler.Handler;
import org.jdbi.v3.core.Jdbi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jndi.JndiTemplate;
import org.springframework.security.crypto.argon2.Argon2PasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.naming.NamingException;
import javax.sql.DataSource;
import java.util.Map;

@Configuration(proxyBeanMethods = false)
@ComponentScan("org.example.app")
public class ApplicationConfiguration {
    @Bean
    public JndiTemplate jndiTemplate() {
        return new JndiTemplate();
    }

    @Bean
    public PasswordEncoder encoder() {
        return new Argon2PasswordEncoder();
    }

    @Bean
    public Gson gson() {
        return new Gson();
    }

    @Bean
    public DataSource dataSource(final JndiTemplate template) throws NamingException {
        return template.lookup("java:/comp/env/jdbc/db", DataSource.class);
    }

    @Bean
    public Jdbi jdbi(final DataSource dataSource) {
        return Jdbi.create(dataSource);
    }

    @Bean(ContextBeanNames.ROUTES)
    public Map<String, Handler> routes(
            final TicketController ticketController,
            final CommentController commentController
    ) {
          return Maps.of("/comments", commentController::seeTicketComments,
                "/ticket", ticketController::create,
                "/tickets", ticketController::getAll,
                "/ticket/close", ticketController::closeTicket,
                "/ticket/comment", commentController::create);
    }
}
