package org.example.app.service;

import lombok.RequiredArgsConstructor;
import org.example.app.dto.CommentRQ;
import org.example.app.dto.CommentRS;
import org.example.app.entity.CommentEntity;
import org.example.app.entity.TicketEntity;
import org.example.app.exception.ItemNotFoundException;
import org.example.app.exception.NoOpportunityForAction;
import org.example.app.exception.TickerIsClosedException;
import org.example.app.mapper.CommentEntityMapper;
import org.example.app.repository.CommentRepository;
import org.example.app.repository.TicketRepository;
import org.example.framework.authentication.Authentication;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@RequiredArgsConstructor
@Component
public class CommentService {
    private final CommentRepository commentRepository;
    private final TicketRepository ticketRepository;
    private final CommentEntityMapper commentEntityMapper;


    public CommentRS create(final CommentRQ requestDTO, final Authentication authentication) {
        final TicketEntity ticketEntity = ticketRepository.getById(requestDTO.getTicketId()).orElseThrow(() -> new ItemNotFoundException("There are no tickets with id: "+requestDTO.getTicketId()));
        if (ticketEntity.getStatus().equals("CLOSED")){
            throw new TickerIsClosedException("Not possible to write comments here");
        }
        if(ticketEntity.getUserLogin().equals(authentication.getLogin())||
                authentication.getRole().equals("support")){
            String userLogin = ticketEntity.getUserLogin();
            final CommentEntity commentEntity = commentEntityMapper.fromCommentRQ(requestDTO,userLogin);
            commentRepository.add(commentEntity);
            return commentEntityMapper.toCommentRS(commentEntity);
        }
        else{
            throw new NoOpportunityForAction("Only author and support can comment this ticket");
        }
    }

    public List<CommentRS> seeTicketComments(final long id, final Authentication authentication) {
        final TicketEntity ticketEntity = ticketRepository.getById(id).orElseThrow(() -> new ItemNotFoundException("There are no tickets with id: "+id));
        if(ticketEntity.getUserLogin().equals(authentication.getLogin())||
                authentication.getRole().equals("support")){
            final List<CommentEntity> comments;
            comments = commentRepository.getByTicketId(id);
            if (!comments.isEmpty()) {
                List<CommentRS> commentsOfCurrentTicket = comments.
                        stream().map(o -> commentEntityMapper.toCommentRS(o))
                        .collect(Collectors.toList());
                return commentsOfCurrentTicket;
            } else {
                throw new ItemNotFoundException("There are no comments of this ticket");
            }
        }
        else{
            throw new NoOpportunityForAction("Only author and support can see comments of this ticket");
        }
    }
}

