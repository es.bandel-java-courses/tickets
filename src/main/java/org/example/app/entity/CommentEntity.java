package org.example.app.entity;
import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Data

public class CommentEntity {
    private long id;
    private long ticketId;
    private String text;
    private String userLogin;
}

