package org.example.framework.listener;

import jakarta.servlet.ServletContextEvent;
import jakarta.servlet.ServletContextListener;
import org.example.framework.attribute.ContextAttributes;
import org.example.framework.exception.ContextInitializeException;
import org.jdbi.v3.core.Jdbi;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotatedBeanDefinitionReader;
import org.springframework.context.support.GenericApplicationContext;

import javax.naming.InitialContext;
import javax.sql.DataSource;

public class ContextLoadDestroyListener implements ServletContextListener {
  private ConfigurableApplicationContext springContext;
  @Override
  public void contextInitialized(ServletContextEvent sce) {
    try {
      final InitialContext initialContext = new InitialContext();
      final DataSource dataSource = (DataSource) initialContext.lookup("java:comp/env/jdbc/db");
      final Jdbi jdbi = Jdbi.create(dataSource);
      sce.getServletContext().setAttribute("jdbi", jdbi);

      final String configClazz = sce.getServletContext().getInitParameter("org.example.app.configuration.ApplicationConfiguration");
      final Class<?> clazz = Class.forName(configClazz);

      final GenericApplicationContext springContext = new GenericApplicationContext();
      final AnnotatedBeanDefinitionReader reader = new AnnotatedBeanDefinitionReader(springContext);
      reader.register(clazz);

      springContext.refresh();
      this.springContext = springContext;

      sce.getServletContext().setAttribute(
              ContextAttributes.ATTR_SPRING_CONTEXT,
              springContext
      );
    } catch (Exception e) {
      throw new ContextInitializeException();
    }
  }


  @Override
  public void contextDestroyed(ServletContextEvent sce) {
    ServletContextListener.super.contextDestroyed(sce);
    if (springContext != null) {
      springContext.close();
    }
  }
}
