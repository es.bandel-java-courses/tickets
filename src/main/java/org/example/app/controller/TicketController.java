package org.example.app.controller;

import com.google.gson.Gson;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.example.app.dto.TicketRQ;
import org.example.app.dto.TicketRS;
import org.example.app.dto.TicketToCloseRQ;
import org.example.app.service.TicketService;
import org.example.framework.authentication.Authentication;
import org.example.framework.exception.AuthenticationException;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;


@RequiredArgsConstructor
@Component
public class TicketController {
    private final TicketService ticketService;
    private final Gson gson;

    public void create(final HttpServletRequest req, final HttpServletResponse resp) throws IOException, AuthenticationException {
        final Authentication authentication = (Authentication) req.getAttribute(Authentication.ATTR_AUTH);
        final TicketRQ requestDTO = gson.fromJson(req.getReader(), TicketRQ.class);
        final TicketRS responseDTO = ticketService.create(requestDTO, authentication);
        final String responseBody = gson.toJson(responseDTO);

        resp.getWriter().write(responseBody);
    }

    public void getAll(final HttpServletRequest req, final HttpServletResponse resp) throws IOException, AuthenticationException {
        final Authentication authentication = (Authentication) req.getAttribute(Authentication.ATTR_AUTH);
        final TicketRQ requestDTO = gson.fromJson(req.getReader(), TicketRQ.class);
        final List<TicketRS> responseDTO = ticketService.getAll(requestDTO, authentication);
        final String responseBody = gson.toJson(responseDTO);
        resp.getWriter().write(responseBody);
    }

    public void closeTicket(final HttpServletRequest req, final HttpServletResponse resp) throws IOException, AuthenticationException {
        final Authentication authentication = (Authentication) req.getAttribute(Authentication.ATTR_AUTH);
        final TicketToCloseRQ requestDTO = gson.fromJson(req.getReader(), TicketToCloseRQ.class);
        ticketService.update(requestDTO, authentication);
        resp.getWriter().write("Ticket with id "+requestDTO.getId()+" is CLOSED");
    }

}