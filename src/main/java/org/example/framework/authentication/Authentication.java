package org.example.framework.authentication;

public interface Authentication {
  String ATTR_AUTH = "org.example.framework.authentication.Authentication";

  String getLogin();
  String getPassword();

  String getRole();

  boolean isAnonymous();

  void eraseCredentials();
}
