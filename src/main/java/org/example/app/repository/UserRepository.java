package org.example.app.repository;

import lombok.RequiredArgsConstructor;
import org.example.app.entity.UserEntity;
import org.jdbi.v3.core.Jdbi;
import org.springframework.stereotype.Component;

import java.util.Optional;

@RequiredArgsConstructor
@Component
public class UserRepository {

    private final Jdbi jdbi;

    public Optional<UserEntity> getByLogin(final String login) {
        return jdbi.withHandle(handle -> handle.createQuery(
                        // language=PostgreSQL
                        "SELECT login, password, role FROM users WHERE login = :login"
                )
                .bind("login", login)
                .mapToBean(UserEntity.class)
                .findFirst());
    }
}
