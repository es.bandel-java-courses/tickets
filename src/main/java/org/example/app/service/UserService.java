package org.example.app.service;

import lombok.RequiredArgsConstructor;
import org.example.app.repository.UserRepository;
import org.example.framework.authentication.Authentication;
import org.example.framework.authentication.LoginPasswordAuthentication;
import org.example.framework.authenticator.Authenticator;
import org.example.framework.exception.AuthenticationException;
import org.example.framework.exception.UserNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;



@RequiredArgsConstructor
@Component
public class UserService implements Authenticator {
    private final UserRepository userRepository;
    private  final PasswordEncoder encoder;
    @Override
    public Authentication authenticate(String login, String password) throws AuthenticationException {
        return userRepository.getByLogin(login)
                .filter(a -> encoder.matches(password,a.getPassword()))
                .map(a -> new LoginPasswordAuthentication(a.getLogin(), a.getPassword(), a.getRole()))
                .orElseThrow(UserNotFoundException::new);
    }
}
