package org.example.framework.exception;

public class ContextInitializeException extends RuntimeException{
    public ContextInitializeException() {
    }

    public ContextInitializeException(String message) {
        super(message);
    }

    public ContextInitializeException(String message, Throwable cause) {
        super(message, cause);
    }

    public ContextInitializeException(Throwable cause) {
        super(cause);
    }

    public ContextInitializeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
