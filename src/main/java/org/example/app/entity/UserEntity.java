package org.example.app.entity;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString

public class UserEntity {

    private long id;
    private String login;
    private String password;
    private String role;
}
