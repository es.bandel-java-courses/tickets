package org.example.app.controller;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.example.app.service.UserService;
import org.example.framework.authentication.Authentication;
import org.example.framework.exception.AuthenticationException;
import org.springframework.stereotype.Component;

import java.io.IOException;

@RequiredArgsConstructor
@Component
public class UserController {

  private final UserService Userservice;

  public void me(final HttpServletRequest req, final HttpServletResponse resp) throws IOException, AuthenticationException {
    final Authentication authentication = (Authentication) req.getAttribute(Authentication.ATTR_AUTH);
    resp.getWriter().write("me: " + authentication.getLogin()+" "+authentication.getRole());
  }
}
