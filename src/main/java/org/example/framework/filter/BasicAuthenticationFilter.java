package org.example.framework.filter;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.example.app.configuration.ApplicationConfiguration;
import org.example.framework.attribute.ContextAttributes;
import org.example.framework.authentication.Authentication;
import org.example.framework.authenticator.Authenticator;
import org.example.framework.exception.BadEncodedBasicCredentials;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;


import java.nio.charset.StandardCharsets;
import java.util.Base64;

public class BasicAuthenticationFilter extends AbstractAuthenticationFilter {
  public static final String AUTHORIZATION = "Authorization";
  public static final String SCHEME = "Basic";
  public static final String DELIMITER = ":";
  private transient Authenticator authenticator;

  @Override
  public void init() throws ServletException {
    ApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfiguration.class);
    authenticator = context.getBean(Authenticator.class);
  }

  @Override
  protected void doApply(final HttpServletRequest req, final HttpServletResponse res, final FilterChain chain) throws Exception {
    final String header = req.getHeader(AUTHORIZATION);
    if (header == null) {
      chain.doFilter(req, res);
      return;
    }

    final String encodedLoginAndPassword = header.substring(SCHEME.length() + 1);
    final String loginAndPassword = new String(Base64.getDecoder().decode(encodedLoginAndPassword), StandardCharsets.UTF_8);
    final String[] parts = loginAndPassword.split(DELIMITER);
    if (parts.length != 2) {
      throw new BadEncodedBasicCredentials();
    }

    final String login = parts[0];
    final String password = parts[1];

    final Authentication authentication = authenticator.authenticate(login, password);
    req.setAttribute(Authentication.ATTR_AUTH, authentication);
    chain.doFilter(req, res);
  }
}
