package org.example.app.exception;

public class NoOpportunityForAction extends RuntimeException{
    public NoOpportunityForAction() {
    }

    public NoOpportunityForAction(String message) {
        super(message);
    }

    public NoOpportunityForAction(String message, Throwable cause) {
        super(message, cause);
    }

    public NoOpportunityForAction(Throwable cause) {
        super(cause);
    }

    public NoOpportunityForAction(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
