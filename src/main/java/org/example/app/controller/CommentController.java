package org.example.app.controller;

import com.google.gson.Gson;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.example.app.dto.CommentRQ;
import org.example.app.dto.CommentRS;
import org.example.app.service.CommentService;
import org.example.framework.authentication.Authentication;
import org.example.framework.exception.AuthenticationException;

import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;


@RequiredArgsConstructor
@Component
public class CommentController {

    private final CommentService commentService;
    private final Gson gson;

    public void create(final HttpServletRequest req, final HttpServletResponse resp) throws IOException, AuthenticationException {
        final Authentication authentication = (Authentication) req.getAttribute(Authentication.ATTR_AUTH);
        final CommentRQ requestDTO = gson.fromJson(req.getReader(), CommentRQ.class);
        final CommentRS responseDTO=commentService.create(requestDTO, authentication);
        final String responseBody = gson.toJson(responseDTO);
        resp.getWriter().write(responseBody);
    }

    public void seeTicketComments(final HttpServletRequest req, final HttpServletResponse resp) throws IOException, AuthenticationException {
        final Authentication authentication = (Authentication) req.getAttribute(Authentication.ATTR_AUTH);
        final long id=Long.parseLong(req.getParameter("id"));
        final List<CommentRS> responseDTO=commentService.seeTicketComments(id, authentication);
        final String responseBody = gson.toJson(responseDTO);
        resp.getWriter().write(responseBody);
    }
}
