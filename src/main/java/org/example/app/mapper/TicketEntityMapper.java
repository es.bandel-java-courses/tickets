package org.example.app.mapper;


import org.example.app.dto.TicketRQ;
import org.example.app.dto.TicketRS;
import org.example.app.entity.TicketEntity;
import org.mapstruct.Mapper;

@Mapper
public interface TicketEntityMapper {
  TicketEntity fromTicketRQ(
      final TicketRQ request,
      final String userLogin
  );

  TicketRS toTicketRS(final TicketEntity entity);

}
