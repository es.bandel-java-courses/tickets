package org.example.framework.authentication;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class LoginPasswordAuthentication implements Authentication {
  private String login;
  private String password;

  private String role;

  @Override
  public String getLogin() {
    return login;
  }
  @Override
  public String getPassword() {
    return password;
  }
  @Override
  public String getRole() {
    return role;
  }

  @Override
  public boolean isAnonymous() {
    return false;
  }

  @Override
  public void eraseCredentials() {
    password = null;
  }
}
