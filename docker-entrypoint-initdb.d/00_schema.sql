CREATE TABLE users
(
    login    TEXT PRIMARY KEY,
    password TEXT NOT NULL,
    role     TEXT NOT NULL
);

CREATE TABLE tickets
(
    id      BIGSERIAL PRIMARY KEY,
    name    TEXT NOT NULL,
    content TEXT NOT NULL,
    status  TEXT DEFAULT 'OPEN',
    userLogin  TEXT REFERENCES users (login)
);

CREATE TABLE comments
(
    id      BIGSERIAL PRIMARY KEY,
    ticketId  BIGINT REFERENCES tickets (id),
    text TEXT NOT NULL,
    userLogin  TEXT REFERENCES users (login)
);
