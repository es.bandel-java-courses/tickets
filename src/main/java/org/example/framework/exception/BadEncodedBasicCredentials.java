package org.example.framework.exception;

public class BadEncodedBasicCredentials extends AuthenticationException {
  public BadEncodedBasicCredentials() {
  }

  public BadEncodedBasicCredentials(String message) {
    super(message);
  }

  public BadEncodedBasicCredentials(String message, Throwable cause) {
    super(message, cause);
  }

  public BadEncodedBasicCredentials(Throwable cause) {
    super(cause);
  }

  public BadEncodedBasicCredentials(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }
}
