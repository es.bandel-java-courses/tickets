package org.example.app.service;

import lombok.RequiredArgsConstructor;
import org.example.app.dto.TicketRQ;
import org.example.app.dto.TicketRS;
import org.example.app.dto.TicketToCloseRQ;
import org.example.app.entity.TicketEntity;
import org.example.app.exception.ItemNotFoundException;
import org.example.app.exception.NoOpportunityForAction;
import org.example.app.mapper.TicketEntityMapper;
import org.example.app.repository.TicketRepository;
import org.example.framework.authentication.Authentication;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Component
public class TicketService {

    private final TicketRepository ticketRepository;

    private final TicketEntityMapper mapper;

    public TicketRS create(final TicketRQ requestDTO, Authentication authentication) {
        if (authentication.getRole().equals("support")) {
            throw new NoOpportunityForAction("Support can't create a ticket");
        }
        final TicketEntity ticket = mapper.fromTicketRQ(requestDTO, authentication.getLogin());
        ticketRepository.add(ticket);
        return mapper.toTicketRS(ticket);
    }

    public void update(final TicketToCloseRQ requestDTO, Authentication authentication) {
        if (authentication.getRole().equals("support")) {
            throw new NoOpportunityForAction("Support can't close tickets");
        }
        final Optional<TicketEntity> ticketEntity = ticketRepository.getById(requestDTO.getId());
        if(ticketEntity.get().getUserLogin().equals(authentication.getLogin())){
            ticketRepository.update(requestDTO.getId());
        }
        else{
            throw new NoOpportunityForAction("Only author can close ticket");
        }
    }

    public List<TicketRS> getAll(final TicketRQ requestDTO, final Authentication authentication) {
        final List<TicketEntity> tickets;
        tickets = ticketRepository.getAll();
        if (!tickets.isEmpty()) {
            if (authentication.getRole().equals("support")) {
                return tickets.stream().map(o -> mapper.toTicketRS(o))
                        .collect(Collectors.toList());
            }
            List<TicketRS> ticketsOfCurrentUser = tickets.
                    stream().filter(ticket -> ticket.getUserLogin().equals(authentication.getLogin()))
                    .map(o -> mapper.toTicketRS(o))
                    .collect(Collectors.toList());
            return ticketsOfCurrentUser;
        } else {
            throw new ItemNotFoundException("There are no tickets!");
        }

    }

}
