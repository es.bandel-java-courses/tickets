package org.example.framework.servlet;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.example.app.configuration.ApplicationConfiguration;
import org.example.framework.attribute.ContextAttributes;
import org.springframework.context.ApplicationContext;
import org.example.framework.context.ContextBeanNames;
import org.example.framework.handler.Handler;
import org.jdbi.v3.core.Jdbi;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;


import java.io.IOException;
import java.util.Map;
import java.util.Optional;


public class Servlet extends HttpServlet {
    private Jdbi jdbi;
    private final transient Handler notFoundHandler = (req, resp) -> resp.sendError(404);
    private final transient Handler internalServerErrorHandler = (req, resp) -> resp.sendError(500);
    private transient Map<String, Handler> routes;

    @Override
    public void init() {
        jdbi = (Jdbi) getServletContext().getAttribute("jdbi");
        final ApplicationContext context = (ApplicationContext) getServletContext().getAttribute(ContextAttributes.ATTR_SPRING_CONTEXT);
        routes = (Map<String, Handler>) context.getBean(ContextBeanNames.ROUTES);
    }

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        final String path = req.getRequestURI().substring(req.getContextPath().length());
        final Handler handler = Optional.ofNullable(routes.get(path))
                .orElse(notFoundHandler);
        try {

            handler.handle(req, resp);
        } catch (Exception e) {
            try {
                internalServerErrorHandler.handle(req, resp);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }
}
