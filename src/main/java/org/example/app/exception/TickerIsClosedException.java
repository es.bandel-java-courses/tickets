package org.example.app.exception;

public class TickerIsClosedException extends RuntimeException{
    public TickerIsClosedException() {
    }

    public TickerIsClosedException(String message) {
        super(message);
    }

    public TickerIsClosedException(String message, Throwable cause) {
        super(message, cause);
    }

    public TickerIsClosedException(Throwable cause) {
        super(cause);
    }

    public TickerIsClosedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
